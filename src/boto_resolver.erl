%% @doc Behaviour that defines a resolver for boto.
%% @end
-module(boto_resolver).

-export_type([name/0, resolver/0, input/0, input_attribute/0, output/0, output_attribute/0, t/0]).

-type name() :: atom(). %% Name of a resolver.

-type resolver() :: [input() | output()]. %% Resolver settings.
%% {@link input()} and {@link output()} must be provided.

-type input() :: {input, [] | list(input_attribute())}. %% Input option.

-type input_attribute() :: atom(). %% Input attribute, those will be required to call the resolver.

-type output() :: {output, list(output_attribute())}.%% Output option.

-type output_attribute() :: atom() | {atom(), list(output_attribute())}. %% Output attribute, those will be provided by the resolver after it's called.
%% A nested attribute shouldn't require another resolver to be called.

-type t() :: list({name(), resolver()}). %% {@link resolver_init()} return, list all resolvers provided by the module implementing this behaviour.

-callback resolver_init() -> t(). %% This will be called at {@link boto_server} start to setup and index all available attributes.

-callback resolve(name(), map()) -> map() | [map()]. %% This function is called when resolving a query.
%% This function will be called only when {@link input_attribute()} are available and the query needs one of the {@link output_attribute()} either to finish traversing a required resolver or for the final output of the query.
